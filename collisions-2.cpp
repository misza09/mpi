#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector> 
#include <set>
#include <cmath>
#include <cfloat>
#include <iomanip>
#include <assert.h>
#include <mpi.h>
#include <stddef.h>  
using namespace std;

#define STARS_AMOUNT_TAG 1
#define STARS_TAG 2
#define STARS_AMOUNT_TAG_V 3
#define STARS_TAG_V 4




#define G 155893.597

#define MP make_pair


typedef struct star_s{
  float x;
  float y;
  float v_x;
  float v_y;
  float a_x;
  float a_y;
  int index;
} star;

typedef struct force_s{
  float x;
  float y;
} force;

MPI_Datatype mpi_star_type;


bool compare_stars(const star &a, const star &b)
{
    return a.index < b.index;
}

inline bool operator<(const star& lhs, const star& rhs)
{
  return lhs.index < rhs.index;
}


float sign(float x){
  if (x >= 0) 
    return 1;
  return -1;
}

int get_value(float value[], int number_of_stars1, int star_id){
  if (star_id < number_of_stars1)
    return value[0];
  return value[1];

}

bool is_number(string s){
  return s.find_first_not_of( "-0123456789" ) == string::npos;
}

void print_galaxies(int number_of_stars[], int myRank, int numProcesses, string filename1, string filename2, vector<star> my_stars ){

  int stars_amount;

  if(myRank == 0){
    set<star> stars_to_print;

    stars_to_print.insert(my_stars.begin(), my_stars.end());


    for(int i = 1; i < numProcesses; i++){
      MPI_Recv(&stars_amount, 1, MPI_INT, i, STARS_AMOUNT_TAG_V, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      star recv_data[stars_amount];
      MPI_Recv(&recv_data, stars_amount, mpi_star_type, i, STARS_TAG_V, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      stars_to_print.insert(recv_data, recv_data+stars_amount);
    }
    //PRINT TO FILE
    ofstream myfile;

    myfile.open(filename1);
    set<star>::iterator it=stars_to_print.begin();
    myfile << std::fixed << std::setprecision(1);
    for(int i = 0; i < number_of_stars[0]; it++, i++){
        myfile  << (*it).x << " " << (*it).y << "\n";
    }
    myfile.close();

    myfile.open(filename2);
    for(;it !=  stars_to_print.end(); it++){
        myfile  << (*it).x << " " << (*it).y<< "\n";
    }
    myfile.close();

  }
  else{
    stars_amount = my_stars.size();
    MPI_Send(&stars_amount, 1, MPI_INT, 0, STARS_AMOUNT_TAG_V, MPI_COMM_WORLD);
    MPI_Send(&my_stars.front(), stars_amount, mpi_star_type, 0, STARS_TAG_V, MPI_COMM_WORLD);
  }
  
}

int get_process_id(float pos_x, float pos_y, float start_x, float end_x, float start_y, float end_y, int hor, int ver){

  if(start_y == end_y && start_x == end_x)
    return 0;
  if(start_y == end_y)
    return  floor(((pos_x - start_x) * hor) / (end_x - start_x));
  if(start_x == end_x)
      return floor(((pos_y - start_y) * ver) / (end_y - start_y))*hor;
  return floor(((pos_y - start_y) * ver) / (end_y - start_y))*hor + floor(((pos_x - start_x) * hor) / (end_x - start_x));

}

int main(int argc, char * argv[])
{
  int numProcesses = 0;
  int myRank = 0;

  int hor=-1, ver=-1;
  float delta = -1, total= -1;
  string gal1_file = "", gal2_file = "";
  int all_stars, number_of_stars[2];
  float speedx[2], speedy[2];
  float mass[2];
  vector<star> positions;
  bool verbose_mode = false;
  float start_x, start_y, end_x, end_y;


  if (argc <= 12)
  {
    fprintf(stderr, "Usage: %s --hor <hor> --ver <ver> --gal1 <file1> --gal2 <file2> --delta <value> --total <value> [-v]\n", argv[0]);
    return 1;
  }

  for (int i = 1; i < argc; ++i)
  {

    if (string(argv[i]) == "--hor")
    {
      if (hor != -1){
        fprintf(stderr, "Multiple hor arguments\n");
        return 1;
      }
      if (!is_number(argv[++i])){
        fprintf(stderr, "Inappropriate hor value\n");
        return 1;
      }
      hor = stoi(argv[i]);
    }

    else if (string(argv[i]) == "--ver"){
      if (ver != -1){
        fprintf(stderr, "Multiple ver arguments\n");
        return 1;
      }
      //fprintf(stderr,"VER %d\n",stoi(argv[i+1] );
      if (!is_number(argv[++i])){
        fprintf(stderr, "Inappropriate ver value\n");
        return 1;
      }
      ver = stoi(argv[i]);
    }


    //TODO 
    else if (string(argv[i]) == "--delta" && delta == -1)
    {
      delta = atof(argv[++i]);
    }
    else if (string(argv[i]) == "--total" && total == -1)
    {
      total = atof(argv[++i]);
    }
    else if (string(argv[i]) == "--gal1" && gal1_file == "")
    {
      gal1_file = argv[++i];
    }
    else if (string(argv[i]) == "--gal2" && gal2_file == "")
    {
      gal2_file = argv[++i];
    }
    else if (string(argv[i]) == "-v")
    {
    verbose_mode = true;
    }
  }


  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  if (hor * ver != numProcesses){
        fprintf(stderr, "Number of processes is not equal to hor*ver\n");
        MPI_Finalize();
        return 1;
  }


  string line;
  float max_top, max_bottom, max_left, max_right;
  
  
  ifstream myfile1 (gal1_file);
  ifstream myfile2 (gal2_file);

  if (myRank == 0){
    int idx;
    float r[2];
    if (myfile1.is_open())
    {
      getline (myfile1, line);
      number_of_stars[0] = stoi(line);
      getline (myfile1, line);
      istringstream iss(line);
      iss >> speedx[0] >> speedy[0];
      getline (myfile1, line);
      mass[0] = stof(line);
      idx = 0;
      for (int i = 0; i < number_of_stars[0]; i++)
      {
        getline (myfile1,line);
        istringstream iss(line);
        iss >> r[0] >> r[1];
        positions.push_back({r[0], r[1], speedx[0], speedy[0], 0, 0, idx});
        idx++;
      }
      myfile1.close();
    }
    else
      fprintf(stderr, "Unable to open file 1\n");


    if (myfile2.is_open())
    {
      getline (myfile2, line);
      number_of_stars[1] = stoi(line);
      getline (myfile2, line);
      istringstream iss(line);
      iss >> speedx[1] >> speedy[1];
      getline (myfile2, line);
      mass[1] = stof(line);
      for (int i = 0; i < number_of_stars[1]; i++)
      {
        getline (myfile2,line);
        istringstream iss(line);
        iss >> r[0] >> r[1];
        positions.push_back({r[0], r[1], speedx[0], speedy[0], 0, 0, idx});
        idx++;
      }
      myfile2.close();
    }
    else
      fprintf(stderr, "Unable to open file 2\n");

    all_stars = number_of_stars[0] + number_of_stars[1];



    max_left = positions[0].x;
    max_right = positions[0].x;
    max_top = positions[0].y;
    max_bottom = positions[0].y;

    for(int i = 1; i < all_stars; i++){
      max_left = min(max_left, positions[i].x);
      max_right = max(max_right, positions[i].x);
      max_top = max(max_top, positions[i].y);
      max_bottom = min(max_bottom, positions[i].y);
    }


    start_x = max_left - (max_right - max_left)/2.0;
    end_x = max_right + (max_right - max_left)/2.0;
    start_y = max_bottom - (max_top - max_bottom)/2.0;
    end_y = max_top + (max_top - max_bottom)/2.0;
  }
  MPI_Bcast(&number_of_stars, 2, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&mass, 2, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&start_x, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&end_x, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&start_y, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
  MPI_Bcast(&end_y, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);



  const int nitems = 7;
  int blocklengths[7] = {1,1,1,1,1,1,1};
  MPI_Datatype types[7] = {MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT, MPI_INT};
  MPI_Aint     offsets[7];

  offsets[0] = offsetof(star, x);
  offsets[1] = offsetof(star, y);
  offsets[2] = offsetof(star, v_x);
  offsets[3] = offsetof(star, v_y);
  offsets[4] = offsetof(star, a_x);
  offsets[5] = offsetof(star, a_y);
  offsets[6] = offsetof(star, index);


  MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpi_star_type);
  MPI_Type_commit(&mpi_star_type);

  // vector<star> my_stars;
  int stars_amount;
  vector<star> my_stars;
  vector<vector<star> > stars_to_send;

  if(myRank == 0){

    stars_to_send.resize(numProcesses);

    for(int i = 0; i < all_stars; i++){
      float vx =  get_value(speedx, number_of_stars[0], i);
      float vy =  get_value(speedy, number_of_stars[0], i);

      int process_id = get_process_id(positions[i].x, positions[i].y, start_x, end_x, start_y, end_y, hor, ver);
      
      if(process_id == 0 ){
        my_stars.push_back({positions[i].x, positions[i].y, vx, vy, 0, 0, i});
      }
      else
        stars_to_send[process_id].push_back({positions[i].x, positions[i].y, vx, vy, 0, 0, i});
    }


    for(int i = 1; i < numProcesses; i++){
      stars_amount = stars_to_send[i].size();
      stars_to_send[i].data(); 
      MPI_Send(&stars_amount, 1, MPI_INT, i, STARS_AMOUNT_TAG, MPI_COMM_WORLD);
      MPI_Send(&stars_to_send[i].front(), stars_amount, mpi_star_type, i, STARS_TAG, MPI_COMM_WORLD);
    }
  }
  else{

    MPI_Recv(&stars_amount, 1, MPI_INT, 0, STARS_AMOUNT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    star recv_data[stars_amount];
    MPI_Recv(&recv_data, stars_amount, mpi_star_type, 0, STARS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    my_stars.insert(my_stars.end(), &recv_data[0], &recv_data[stars_amount]);
  }

  // Rozesłanie informacji o swoich gwiazdach celem policzenia sił przez inne procesy


  for (int step = 0; step < total/delta; step++){
      
    vector<force> forces;
    forces.clear();
    forces.resize(my_stars.size());
    vector<bool>stay;
    stay.clear();
    stay.resize(my_stars.size(), false);





//VERBOSE_MODE PRINT_STARS

    if (verbose_mode){
      string filename1 = "res1_" + to_string(step) + ".txt";      
      string filename2 = "res2_" + to_string(step) + ".txt";
      print_galaxies(number_of_stars, myRank, numProcesses, filename1, filename2, my_stars);
    }

    
        // SEND MY STAR TO OTHER PROCESSES TO COMPUTE THEIR IMPACT AND FORCES
    vector<star> stars;
    MPI_Status status;

    set<int> neighbours;
    int upper = myRank - hor;
    if (upper < 0){
      upper += hor*ver;
    }
    neighbours.insert(upper);

    int ul = upper - 1;
    if (ul/hor != upper/hor || ul < 0){
      ul += hor;
    }
    neighbours.insert(ul);

    int ur= upper + 1;
    if (ur/hor != upper/hor){
      ur -= hor;
    }
    neighbours.insert(ur);

    int left =  myRank - 1;
    if (left/hor != myRank/hor || left < 0){
      left += hor;
    }
    neighbours.insert(left);

    int right =  myRank + 1;
    if (right/hor != myRank/hor){
      right -= hor;
    }
    neighbours.insert(right);


    int lower = myRank + hor;
    if (lower >= hor*ver){
      lower -= hor*ver;
    }
    neighbours.insert(lower);

    int ll = lower - 1;
    if (ll/hor != lower/hor || ll < 0){
      ll += hor;
    }
    neighbours.insert(ll);

    int lr= lower + 1;
    if (lr/hor != lower/hor){
      lr -= hor;
    }
    neighbours.insert(lr);

    neighbours.erase (myRank);


    set<int>::iterator it = neighbours.begin();
    for(;*it < myRank && it != neighbours.end(); it++){
      stars_amount = my_stars.size();

      MPI_Send(&stars_amount, 1, MPI_INT, *it, STARS_AMOUNT_TAG, MPI_COMM_WORLD);
      MPI_Send(&my_stars.front(), stars_amount, mpi_star_type, *it, STARS_TAG, MPI_COMM_WORLD);
    }

    set<int>::iterator send_it=neighbours.begin();
    for(;send_it != neighbours.end(); send_it++){
      MPI_Recv(&stars_amount, 1, MPI_INT, *send_it, STARS_AMOUNT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      star recv_data[stars_amount];
      MPI_Recv(&recv_data, stars_amount, mpi_star_type, *send_it, STARS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      stars.insert(stars.end(), &recv_data[0], &recv_data[stars_amount]);
    }

    for(;it != neighbours.end(); it++){

      stars_amount = my_stars.size();
      MPI_Send(&stars_amount, 1, MPI_INT, *it, STARS_AMOUNT_TAG, MPI_COMM_WORLD);
      MPI_Send(&my_stars.front(), stars_amount, mpi_star_type, *it, STARS_TAG, MPI_COMM_WORLD);
    }
    stars.insert(stars.end(), my_stars.begin(), my_stars.end());


    for (int j = 0; j < my_stars.size(); j++){//my_stars.size(); i++){
      float f_x = 0;//forces[j].x;
      float f_y = 0;//forces[j].y;
      float f;
      float mass1 = get_value(mass, number_of_stars[0], my_stars[j].index);
      for (int k = 0; k < stars.size() && !stay[j];k++){
        if(my_stars[j].index == stars[k].index)
          continue;
        float mass2 = get_value(mass, number_of_stars[0], stars[k].index);

        float dist_x = my_stars[j].x - stars[k].x;
        float dist_y = my_stars[j].y - stars[k].y;
        f = (G * mass1 * mass2) /((dist_x * dist_x) + (dist_y * dist_y));
        if ((dist_x == 0 && dist_y == 0) || f > (FLT_MAX/2)){
          stay[j] = true;
          f_x = f_y = 0;
          forces[j] = force({f_x, f_y});
          break;
        }
        else if (dist_x == 0){
          f_y -= f * sign(dist_y);
        }
        else if(dist_y == 0){
          f_x -= f * sign(dist_x);
        }
        else{
          f_x -= dist_x * sqrt(f*f  / (dist_x*dist_x + dist_y*dist_y));
          f_y -= dist_y * sqrt(f*f  / (dist_x*dist_x + dist_y*dist_y));
        }

      }        

      f_x +=forces[j].x;
      f_y +=forces[j].y;
      forces[j] = force({f_x, f_y});
    }      
    

    //Policz przyspieszenie
    stars_to_send.clear();
    stars_to_send.resize(numProcesses);

    float ax, ay, a2_x, a2_y, v2_x, v2_y, r2_x, r2_y;
    for (int i = 0; i < my_stars.size(); i++){
      if (stay[i]){
        a2_x = a2_y = v2_x = v2_y = 0;
        r2_x = my_stars[i].x;
        r2_y = my_stars[i].y;
      }
      else{
        float mass1 = get_value(mass, number_of_stars[0], my_stars[i].index);

        a2_x = forces[i].x/mass1;
        a2_y = forces[i].y/mass1;
        if (step == 0){
          ax = a2_x;
          ay = a2_y;  
        }
        else{
          ax = my_stars[i].a_x;
          ay = my_stars[i].a_y;
        }
        v2_x = my_stars[i].v_x + ((ax + a2_x) * delta)/2;
        v2_y = my_stars[i].v_y + ((ay + a2_y) * delta)/2;
        r2_x = my_stars[i].x + my_stars[i].v_x * delta + (ax/2 * delta*delta);
        r2_y = my_stars[i].y + my_stars[i].v_y * delta + (ay/2 * delta*delta);

        r2_x = fmod(r2_x - start_x, end_x - start_x) + start_x;   
        r2_y = fmod(r2_y - start_y, end_y - start_y) + start_y;
        if (r2_x < start_x)
          r2_x += end_x - start_x;
        if (r2_y < start_y)
          r2_y += end_y - start_y;
      }


      int process_id = get_process_id(r2_x, r2_y, start_x, end_x, start_y, end_y, hor, ver);

      stars_to_send[process_id].push_back({r2_x, r2_y, v2_x, v2_y, a2_x, a2_y, my_stars[i].index});

    }

    my_stars.clear();

    for (int i = 0; i <numProcesses ; i++){
      if (i == myRank){
        my_stars.insert(my_stars.end(),stars_to_send[i].begin() , stars_to_send[i].end());

        for (int j = 0; j < numProcesses; j++){
          if (j != i){
            MPI_Recv(&stars_amount, 1, MPI_INT, j, STARS_AMOUNT_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

            star recv_data[stars_amount];
            MPI_Recv(&recv_data, stars_amount, mpi_star_type, j, STARS_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            my_stars.insert(my_stars.end(), &recv_data[0], &recv_data[stars_amount]);
          }
        } 
      }
      else{

        stars_amount = stars_to_send[i].size();

        MPI_Send(&stars_amount, 1, MPI_INT, i, STARS_AMOUNT_TAG, MPI_COMM_WORLD);
        MPI_Send(&stars_to_send[i].front(), stars_amount, mpi_star_type, i, STARS_TAG, MPI_COMM_WORLD);

      }
    }
  }
  string filename1 = "res1.txt";      
  string filename2 = "res2.txt";

  print_galaxies(number_of_stars, myRank, numProcesses, filename1, filename2, my_stars);
  MPI_Finalize();


  return 0;
}
