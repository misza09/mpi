#
# A template for the 2016 MPI lab at the University of Warsaw.
# Copyright (C) 2016, Konrad Iwanicki.
#
CC          := mpic++
# Available flags:
# -DUSE_RANDOM_GRAPH=1   --- generates a random graph
# -DUSE_RANDOM_SEED=123  --- uses a given seed to generate a random graph
CFLAGS      := -O3 -Wall -c -std=c++11
LFLAGS      := -O3 -Wall
ALL         := collisions-1 collisions-2 collisions-3


all : $(ALL)

collisions-1 : collisions-1.o
	$(CC) $(LFLAGS) -o $@ $^

collisions-2 : collisions-2.o
	$(CC) $(LFLAGS) -o $@ $^

collisions-3 : collisions-3.o
	$(CC) $(LFLAGS) -o $@ $^


%.o : %.cpp Makefile
	$(CC) $(CFLAGS) $<

clean :
	rm -f *.o *.out *.err $(ALL)
